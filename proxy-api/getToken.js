// getToken.js
const axios = require('axios');

async function getToken() {
  // Nombre de usuario y contraseña del usuario para la API
  const username = "tu_id_de_usuario";
  const password = "tu_contraseña";

  // Definir la URL de destino
  const apiUrl = 'https://api-sandbox.controltotal.vip';

  // Codificar las credenciales en Base64
  const credentials = Buffer.from(`${username}:${password}`).toString('base64');

  // Configurar la solicitud con axios
  try {
    const response = await axios.get(`${apiUrl}/auth/sign-in`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${credentials}`,
      },
      params: {
        token_lifetime: 84600,
      },
    });
    return response.data.access_token;
  } catch (error) {
    throw error;
  }
}

module.exports = getToken;
