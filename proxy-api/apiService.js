// apiService.js
const axios = require('axios');

// Definir la URL de la API de ControlTotal
const apiUrl = 'https://api-sandbox.controltotal.vip';

/**
 * Obtener el listado de productos
 * @param {string} accessToken - Token de acceso para autenticación.
 * @returns {Promise<object>} - Listado de productos.
 */

async function getProducts(accessToken) {
  try {
    const response = await axios.get(`${apiUrl}/products/getProducts`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`,
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
}

/**
 * Obtener detalles de un producto.
 * @param {string} accessToken - Token de acceso para autenticación.
 * @param {string} productId - Identificador del producto.
 * @returns {Promise<object>} - Detalles del producto.
 */
async function getProductDetails(accessToken, productId) {
  try {
    const response = await axios.get(`${apiUrl}/products/getProductDetails`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`,
      },
      params: {
        productId: productId,
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
}

/**
 * Obtener detalles de un subproducto.
 * @param {string} accessToken - Token de acceso para autenticación.
 * @param {string} subproductId - Identificador del subproducto.
 * @returns {Promise<object>} - Detalles del subproducto.
 */
async function getSubproductDetails(accessToken, subproductId) {
  try {
    const response = await axios.get(`${apiUrl}/products/getSubproductDetails`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`,
      },
      params: {
        subproductId: subproductId,
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
}

/**
 * Crear una venta mediante la API externa.
 * @param {string} accessToken - Token de acceso para autenticación.
 * @param {object} saleParams - Parámetros de la venta.
 * @returns {Promise<object>} - Resultado de la creación de la venta.
 */
async function createSale(accessToken, saleParams) {
  try {
    const response = await axios.post(`${apiUrl}/sales/create`, saleParams, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`,
      },
    });
    return response.data;
  } catch (error) {
    throw error;
  }
}

module.exports = { getProducts, getProductDetails, getSubproductDetails, createSale };
