// app.js
const express = require('express');
const cors = require('cors');
const getToken = require('./getToken');
const { getProducts, getProductDetails, getSubproductDetails, createSale } = require('./apiService');

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

let accessToken;

// Middleware para obtener el token antes de manejar la ruta /getProducts, /getProductDetails, /getSubproductDetails y /saleCreate
app.use(async (req, res, next) => {
  if (!accessToken) {
    try {
      accessToken = await getToken();
    } catch (error) {
      console.error('Error al obtener el token:', error.message);
      return res.status(500).json({ error: 'Error al obtener el token' });
    }
  }
  next();
});

// Ruta para /getProducts
app.get('/getProducts', async (req, res) => {
  try {
    const products = await getProducts(accessToken);
    res.json(products);
  } catch (error) {
    console.error('Error al obtener productos:', error.message);
    res.status(500).json({ error: 'Error al obtener productos' });
  }
});

// Ruta para /getProductDetails
app.get('/getProductDetails', async (req, res) => {
  const { productId } = req.query;

  try {
    const productDetails = await getProductDetails(accessToken, productId);
    res.json(productDetails);
  } catch (error) {
    console.error('Error al obtener detalles del producto:', error.message);
    res.status(500).json({ error: 'Error al obtener detalles del producto' });
  }
});

// Ruta para /getSubproductDetails
app.get('/getSubproductDetails', async (req, res) => {
  const { subproductId } = req.query;

  try {
    const subproductDetails = await getSubproductDetails(accessToken, subproductId);
    res.json(subproductDetails);
  } catch (error) {
    console.error('Error al obtener detalles del subproducto:', error.message);
    res.status(500).json({ error: 'Error al obtener detalles del subproducto' });
  }
});

// Nueva ruta para /saleCreate
app.post('/saleCreate', async (req, res) => {
  const saleParams = req.body;

  try {
    const saleResult = await createSale(accessToken, saleParams);
    res.json(saleResult);
  } catch (error) {
    console.error('Error al crear la venta:', error.message);
    res.status(500).json({ error: 'Error al crear la venta' });
  }
});

app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});
