# ControlTotal ejemplo de integración API-js

A continuación encontrarás un ejemplo de integración básico para Node.js


Asegúrate de reemplazar los valores de usuario y contraseña en getToken.js

Recuerda también que esto es solo un ejemplo debes personalizarlo y adaptarlo según las características específicas de tu proyecto.

####IMPORTANTE: Este ejemplo no maneja validaciones, ni controla vigencia del token.


El objetivo de este ejemplo es mostrar como se deben crear los endpoints (proxy) que redirigirán internamente las peticiones realizadas a tu aplicacion, hacia los endpoints de ControlTotal, garantizando que tu token de acceso, siempre esté seguro y protegido ya que solo se tendrá acceso a él desde el back de tu plataforma.

## Instalación

Estas son las instrucciones para instalar y configurar el ejemplo en una máquina local.

1. Clona este repositorio:
git clone https://gitlab.com/gurusistemas/controltotal-api-js-node-sample

Ingresa al directorio del proyecto:
cd controltotal-api-js-node-sample

Instala las dependencias:
npm install

Configura las variables de entorno (si es necesario). Puedes crear un archivo .env con la información necesaria, como por ejemplo la URL de la API de ControlTotal.

Este proyecto incluye un servidor Node.js que actúa como intermediario entre tu aplicación web y la API de ControlTotal.

Para ejecutar el servidor:
node app.js
Los endpoints estarán disponibles en: http://localhost:3000.

La interfaz web se encuentra en el archivo html/index.html.

Para servir el archivo index.html, puedes utilizar un servidor web simple. Hay varias opciones disponibles, y una de las más sencillas es utilizar el paquete http-server de npm.

Asumiendo que ya has instalado Node.js y npm, puedes seguir estos pasos:

Instala http-server ejecutando el siguiente comando en tu terminal:

npm install -g http-server
Navega hasta la carpeta que contiene tu archivo index.html usando la terminal:

cd ruta/de/tu/proyecto/html
Ejecuta el siguiente comando para iniciar el servidor:

http-server
Esto iniciará un servidor en el puerto 8080 por defecto.

Abre tu navegador y ve a http://localhost:8080. Deberías ver la página de ejemplo servida a través de este servidor.
